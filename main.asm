%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2
%include 'dict.inc'
%include 'lib.inc'
%include 'words.inc'

section .rodata
found_error: 
    db 'Вхождение не найдено!', 0
length_error: 
    db 'Строка слишком длинная!', 0
   

section .text
global _start
_start:
  sub rsp, BUFFER_SIZE
  mov rdi, rsp
  mov rsi, BUFFER_SIZE
  call read_word
  cmp rax, 0
  je .len_error
  mov rdi, rax
  mov rsi, current
  call find_word
  cmp rax, 0
  je .not_found_error
.found_success:
  	add rax, 8
  	mov rdi, rax
  	push rdi
    	call string_length
    	pop rdi
    	inc rdi
    	add rdi, rax
    	mov rsi, STDOUT
    	call print_string
    	call print_newline
    	mov rdi, 0
    	call exit
.len_error:
    	mov rdi, length_error
    	jmp .end_error
.not_found_error:
    	mov rdi, found_error
    	jmp .end_error
.end_error:
	mov rsi, STDERR
    	call print_string
    	call print_newline
    	mov rdi, 1
    	call exit
