main: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o
	
main.o: main.asm words.inc 
	nasm -f elf64 -o main.o main.asm

dict.o: dict.inc dict.asm 
	nasm -f elf64 -o dict.o dict.asm
	
lib.o: lib.asm
	nasm -f elf64 -o lib.o lib.asm

clean: 
	rm -f *.o main
